// JavaScript Comments
// This is a single line comment [ctrl/cmd + /]
/*
	This is a multi line comment [ctrl + shft + / (windows) || cmd + option + / (mac)]
*/

// Syntax and Statements
/*
	- Statements in programming are instructions that we tell the computer to perform. It usually ends with semicolon (;)
	- A syntax in programming is the set of rules that describes how statements must be constructed
*/

console.log("Hello, world");

// Variables
// It is used to contain data
// Declaring a varible
/* Syntax
	let/const variableName;
*/

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/


let myVariable;

console.log(myVariable);

// console.log(hello);

// let hello;

// Initializing Variables
/* Syntax
	let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
const pi = 3.1416;

// Reassigning Variable Values
/* Syntax:
	variableName = newValue;
*/

productName = 'Laptop';
console.log(productName);

// interest = 4.489;

// Declaring a Variable before initializing a value

let supplier;
supplier = 'John Smith Trading';
console.log(supplier);

// Multiple Variable Declarations

let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand);

// Using a Variable with a reserved keyword
// const let = 'hello';

// console.log(let);
// Uncaught SyntaxError: let is disallowed as a lexically bound name (at index.js:75:7)

// Data Types
/*
Strings - Series of Characters that creates a word, phrase, sentence or anything related to creating text
*/

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings
// Multiple String Values can be combined to create a single string using the "+" symbol

let fullAddress = province+ ', ' + country;
console.log(fullAddress);

// Metro Manila, Philippines

let greeting = 'I live in the ' + country;
console.log(greeting);

// I live in the Philippines

// The Escpate character (\) in strings combination with other charcters can produce different effects
// "\n" refers to creating a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

/*
Metro Manila

Philippines
*/

let message = "John's employees went home early";
console.log(message);

// John's employees went home early

message = 'John\'s employees went home early';
console.log(message);

// John's employees went home early

let dream = 'I am "inevitable"';
console.log(dream);

// I am "inevitable"


// Number

// Integers or Whole numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and numbers
console.log("John's grade last quarter is " + grade);

// John's grade last quarter is 98.7


// Boolean
// Boolean values are normally used to store values relating to the state of certain things

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

/*
isMarried: false
inGoodConduct: true
*/ 


// Arrays
// Arrays aare  special kind of data type that is used to store multiple values

/*Syntax
	let/const arrayName = [elementA, elementB, elementC, ...];

	let grade1 = 98.7;
	let grade2 = 92.1;

*/

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
// (4)[98.7, 92.1, 90.2, 94.6]

// Different Data Types
let details = ["John", "Smith", 32, true]
console.log(details);
// (4)['John', 'Smith', 32, true]

// Object
// Objects are another special kind of data type that is used to mimic real world objects or items
/*
Syntax
	let/const objectName = {
		propertyA: value,
		propertyB: value
	};
*/

let person = {
	name: 'Leonell Cruz',
	age: 28,
	isMarried: false,
	contact: ['+639154208301', '84255191'],
	address: {
		housenumber: '345',
		city: 'Manila'
	}
};

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};

console.log(myGrades);

// Null
// Used intentionally to express absence of a value in a variable

let spouse = null;
let myString = '';

console.log(myString);
console.log(spouse);

let myNumber = 0;
console.log(myNumber);